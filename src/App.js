import React, { Component } from "react";
import fullLogo from "../src/images/full_logo.png";
import home from "../src/images/home.jpg";
import problem from "../src/images/problem.jpg";
import scope from "../src/images/scope.jpg";
import solution from "../src/images/solution.jpg";
import "./App.css";

class App extends Component {
  state = {
    buttonSelected: "home-button"
  };

  handleButton = event => {
    const target = event.target;
    const name = target.name;
    this.setState({
      buttonSelected: name
    });
    console.log(
      this.state.buttonSelected,
      " es de tipo ",
      typeof this.state.buttonSelected
    );
  };

  displayContent() {
    switch (this.state.buttonSelected) {
      case "problem-button":
        return (
          <div className="content">
            <div className="text-display">
              <h2>
                ¿Cuál es el problema que resolverá o la necesidad que el
                emprendimiento solucionará?
              </h2>
              <p>
                En Colombia, actualmente los ciudadanos tienen pocas opciones a
                la hora de contratar un servicio de cualquier tipo (Servicios
                domésticos, arquitectos, abogados, plomeros, cerrajeros,
                carpinteros, ingenieros, diseñadores, etc.):
              </p>
              <ol>
                <li>Contratar a un amigo o conocido.</li>
                <li>
                  Pedir referencias a sus amigos o conocidos para contratar a un
                  desconocido, pero conocido de ellos.
                </li>
                <li>Buscar clasificados en internet o el periódico.</li>
                <li>Recurrir a empresas.</li>
              </ol>
              <p>
                Cada una de estas opciones tiene sus propios inconvenientes:
              </p>
              <ol>
                <li>
                  A menos que tengas un circulo social muy grande, es poco
                  probable que puedas encontrar siempre un amigo o conocido que,
                  además de tener los conocimientos adecuados para lo que se
                  necesita, también tenga la suficiente experiencia y brinde un
                  servicio de calidad.
                </li>
                <li>
                  Seguimos dependiendo de tener un círculo social amplio y
                  además que nuestros conocidos tengan a su vez un circulo
                  social igualmente amplio.
                </li>
                <li>
                  Cualquier persona hoy en día puede tener acceso a internet y
                  es posible que se realicen clasificados malintencionados o
                  fraudulentos y estamos expuestos a confiar en la buena fe de
                  las personas.
                </li>
                <li>
                  Estas empresas pueden tener un costo muy elevado a comparación
                  de personas independientes que puedan realizar esta labor, y
                  de igual o mejor calidad, y generalmente los contratos para
                  estas empresas se limitan a personas con cierta solvencia
                  económica.
                </li>
              </ol>
            </div>
            <div className="image-display">
              <img src={problem} className="content-image" alt="logo" />
            </div>
          </div>
        );
      case "scope-button":
        return (
          <div className="content">
            <div className="text-display">
              <h2>
                ¿Quiénes son los usuarios y/o clientes que tienen ese problema o
                necesidad y la forma en que el emprendimiento interactúa o
                interactuará con ellos?
              </h2>
              <p>
                Los usuarios y clientes que presentan esta problematica son en
                general toda las personas que necesiten cualquier tipo de
                servicio para su hogar, su trabajo, tercerizar alguna tarea
                específica para su empresa o incluso encontrar personal
                capacitado para su empresa.
              </p>
              <p>
                También abarcamos a las personas naturales o jurídicas que
                buscan satisfacer problemáticas de la vida cotidiana o
                empresarial a través de la prestación de servicios. Principal e
                inicialmente considerando únicamente servicios de mano de obra,
                bien sean, arreglos domésticos, diseño gráfico, planeación,
                asesorías, mano de obra en construcción, etc. Estamos hablando
                de cualquier área donde se necesita una o varias personas
                capacitadas en una o varias labores específicas y que son
                requeridos ya sea de manera constante y cotidiana, o por
                contratos específicos para realizar proyectos personales o
                profesionales.
              </p>
              <p>
                Este emprendimiento pretende conectar a los clientes con los
                usuarios que ellos necesitan para satisfacer una necesidad
                específica mediante una plataforma digital que será una página
                web y una aplicación móvil.
              </p>
            </div>
            <div className="image-display">
              <img src={scope} className="content-image" alt="logo" />
            </div>
          </div>
        );
      case "solution-button":
        return (
          <div className="content">
            <div className="text-display">
              <h2>
                ¿Cuál es la propuesta de solución diferencial al problema
                identificado? ¿Ese diferencial es frente a lo existente en el
                mercado y a la forma en que esas personas resuelven su problema
                o satisfacen su necesidad actualmente?
              </h2>
              <p>
                Tiungo Service, permitirá a cualquier persona, de manera global,
                adquirir los servicios que necesitan mediante una aplicación
                móvil y una página web, para evitar estos problemas resaltados
                anteriormente de la siguiente manera:
              </p>
              <ul>
                <li>
                  Los usuarios que tengan conocimientos en cualquier labor, sin
                  importar que tan compleja o que tan simple sea, pueden
                  registrarse de manera gratuita para ofrecer dicho servicio con
                  un alcance global.
                </li>
                <li>
                  Los clientes que requieran dicho servicio pueden registrarse
                  igualmente de manera gratuita y buscar por categorías, por
                  nombre de servicio, usuario, etc. y filtrar todos los usuarios
                  que ofrecen este servicio ya sea por puntuación o precio y
                  contratar al usuario que crean mas conveniente según estos
                  aspectos y la calificación que tenga el usuario.
                </li>
                <li>
                  Los clientes pagan el servicio y se les garantiza devolución
                  del dinero si el usuario no cumple con la labor contratada.
                </li>
                <li>
                  Se generará un espacio donde se supervisará y se implementara
                  un sistema para evitar usuarios malintencionados que ingresen
                  a la misma.
                </li>
                <li>
                  Facilitamos y centralizamos en una sola plataforma, todos los
                  usuarios registrados como prestadores de servicios con todos
                  los clientes registrados para solicitar los servicios. De esta
                  forma, de manera rápida, sencilla, segura y confiable, podemos
                  evitar los problemas antes mencionados.
                </li>
              </ul>
            </div>
            <div className="image-display">
              <img src={solution} className="content-image" alt="logo" />
            </div>
          </div>
        );

      default:
        return (
          <div className="content-home">
            <div className="text-display">
              <h2>
                Bienvenidos a la página provisional de Tiungo Service para la
                convocatoria a emprendedores: Descubrimiento de negocios
                presencial
              </h2>
              <p>
                Ésta página fue diseñada y creada específicamente para la
                convocatoria de Apps.co, con el fin de dar a conocer los
                aspectos más importantes de este proyecto en desarrollo.
              </p>
              <p>
                Por favor seleccione una de las pestañas de la barra de
                navegación superior para conocer más detalles sobre la idea de
                negocio y el por qué decidimos realizar este emprendimiento.
              </p>
            </div>
            <div className="image-display">
              <img src={home} className="content-image" alt="logo" />
            </div>
          </div>
        );
    }
  }

  render() {
    return (
      <div className="App">
        {/* <header className="App-header">
          <img src={fullLogo} className="App-fullLogo" alt="fullLogo" />
          <p>Esta página estará disponible el Lunes 16 de Septiembre de 2019</p>
        </header> */}
        <header className="header">
          <div className="tab">
            <button onClick={this.handleButton} name="home-button">
              <img src={fullLogo} className="imageLogo" alt="fullLogo" />
            </button>
          </div>
          <div className="tab">
            <button onClick={this.handleButton} name="problem-button">
              Problema a resolver
            </button>
          </div>
          <div className="tab">
            <button onClick={this.handleButton} name="scope-button">
              Dirigido a
            </button>
          </div>
          <div className="tab">
            <button onClick={this.handleButton} name="solution-button">
              Solución
            </button>
          </div>
        </header>
        {this.displayContent()}
      </div>
    );
  }
}

export default App;
